#include <array>
#include <cstddef>
#include <cstdint>
#include <filesystem>
#include <vector>

#include <boost/multi_array.hpp>
#include <highfive/H5File.hpp>
#include <vtkDataArray.h>
#include <vtkDataSet.h>
#include <vtkImageData.h>
#include <vtkNIFTIImageHeader.h>
#include <vtkNIFTIImageReader.h>
#include <vtkPointData.h>
#include <vtkSmartPointer.h>

struct grid
{
  boost::multi_array<float, 4> data    {};
  std::array        <float, 3> spacing {};
};

std::unique_ptr<grid> read_nifti(const std::string& filepath)
{
  auto result = std::make_unique<grid>();
  
  auto reader = vtkSmartPointer<vtkNIFTIImageReader>::New();
  reader->SetFileName    (filepath.c_str());
  reader->SetTimeAsVector(true);
  reader->Update         ();
  auto image       = reader->GetOutput     ();
  auto header      = reader->GetNIFTIHeader();
  auto scalar_data = image ->GetPointData  ()->GetScalars(); // The vtkNIFTIImageReader creates multi-component double scalars, even for higher-dimensional data.
  
  auto dimensions = std::array<std::size_t, 4>(); dimensions.fill(1);
  for (std::size_t i = 0; i < header->GetDim(0); ++i)
  {
    dimensions[i] = header->GetDim(i + 1);
    if (i < result->spacing.size())
      result->spacing[i] = header->GetPixDim(i + 1);
  }

  result->data.resize(dimensions); // We use a 4D multi-array of floats, even for scalar fields (which will have a final dimension of 1).
  for (std::size_t i = 0; i < image->GetNumberOfPoints(); ++i)
  {
    auto index      = std::array <std::int32_t, 3>();
    auto parametric = std::array <double      , 3>();
    auto scalars    = std::vector<double>(dimensions[3]);

    image      ->ComputeStructuredCoordinates(image->GetPoint(i), index.data(), parametric.data());
    scalar_data->GetTuple                    (i, scalars.data());

    for (std::size_t j = 0; j < scalars.size(); ++j)
      result->data[index[0]][index[1]][index[2]][j] = scalars[j]; // The data types were converted to double by VTK and are now converted to float by us.
  }

  return result;
}
void                  write_hdf5(const std::string& filepath, const grid* grid)
{
  auto file      = std::make_unique<HighFive::File>(filepath, HighFive::File::Overwrite);
  auto dataset   = file->createDataSet  <float>("data"   , HighFive::DataSpace::From(grid->data   ));
  auto attribute = file->createAttribute<float>("spacing", HighFive::DataSpace::From(grid->spacing));
  dataset  .write(grid->data   );
  attribute.write(grid->spacing);
}

std::int32_t main(std::int32_t argc, char** argv)
{
  auto dataset = read_nifti(argv[1]);
  write_hdf5(std::filesystem::path(argv[1]).replace_extension("h5").string(), dataset.get());
  return 0;
}