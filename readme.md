### NII2HDF
Converts NIFTI 1/2 files to HDF5 files containing an N-dimensional regular grid and a voxel spacing attribute.

### Requirements
- CMake 3.15+
- Git 2.24+
- GCC 9+ or Visual Studio 2019+

### Building
- Clone the repository.
- Run `bootstrap.[sh|bat]`. It takes approximately 15 minutes to download, build and install all dependencies through vcpkg.
- The binaries are then available under the `./build` folder. Run as `nii2hdf [PATH_TO_NII]`.

### Why ditch NIFTI for HDF5?
- NIFTI does not have a specification. It is a pile of code and documentation.
- NIFTI contains many redundant/historical features which are confusing to a newcomer.
- NIFTI has an official implementation, but it is full of bugs.
- NIFTI does not support distributed parallel scenarios, which are becoming the standard in research.
- NIFTI will lead you to cases where you spend a lot of time on IO, time which could instead be spent on valuable research.
- NIFTI is a horrible file format that needs to be buried into dusty pages of history asap.
- Couple this code with a decent HDF5 wrapper for your language and never f**k around with NIFTI again.